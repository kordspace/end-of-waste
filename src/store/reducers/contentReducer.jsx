const initState = {
    content: [
        {id: 'hero', header: '67% OF GLASS WASTE GOES INTO THE LANDFILL EACH YEAR', subheader: 'WHILE CONSUMPTION & WASTE INCREASE', content: 'Will You Choose to Make a Difference?'},
        {id: 'about', header: 'End of Waste', subheader: 'The Future of Recycling', content: 'We are a public service corporation that builds tools to engage the world in a new standard of recycling—one that can help reverse our climate crisis. Connecting contributors across the nation and globe, our network collectively invests in a blockchain circular economy that fuels smart infrastructure, innovative products, living wage jobs and healthier, more resilient communities.'},
        {id: 'consumers', header: 'Welcoming Conscious Product Manufacturers, Consumers and Sustainable Business', subheader: '', content: 'Consumers and businesses can contribute to glass recycling by purchasing EOW™ Glass Certificates for a nominal fee. These certificates are blockchain verified and trace origin of glass. We offer consumers and businesses the opportunity to become early adopters and advocate for Voluntary Shared Responsibility. You can make a real difference and help fuel recycling infrastructure, living wage jobs, healthier communities & climate resilience across the USA.'},
        {id: 'suppliers', header: 'Partnering with MRFs, Glass Processors and Manufacturers', subheader: '', content: 'Waste Suppliers and MRFs desire to be compensated for their hard work for recycling glass items. We aim to reach out to communities across the world to help fund glass recycling so that waste suppliers can feel justified for the time, energy and equipment used for recycling in this budget restricted market areas for the waste management industry.'},
    ]
}

const contentReducer = (state = initState, action) => {
    return state
}

export default contentReducer