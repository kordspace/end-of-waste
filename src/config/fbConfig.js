import firebase from 'firebase/app'
import 'firebase/firestore' 
import 'firebase/auth' 

// Initialize Firebase
var config = {
  apiKey: "AIzaSyByPxpDkIuFq9KEc-ll7fmK4JfnVm602Uc",
  authDomain: "end-of-waste.firebaseapp.com",
  databaseURL: "https://end-of-waste.firebaseio.com",
  projectId: "end-of-waste",
  storageBucket: "end-of-waste.appspot.com",
  messagingSenderId: "916294271232"
};
  firebase.initializeApp(config);
  firebase.firestore().settings({ timestampsInSnapshots: true }); 

  export default firebase;