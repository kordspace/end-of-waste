import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

import descriptionStyle from "assets/jss/material-kit-pro-react/views/aboutUsSections/descriptionStyle.jsx";

function SectionDescription(props) {
  const { classes } = props;
  return (
    <div className={classNames(classes.aboutDescription, classes.textCenter)}>
      <GridContainer>
        <GridItem
          md={8}
          sm={8}
          className={classNames(classes.mrAuto, classes.mlAuto)}
        >
          <h2 className={classes.description}>
            MARKETING IS <b>SACRED</b> TO US…
          </h2><br></br>
          <h5 className={classes.description}>
            …because we promote things that enrich lives for companies that are changing the world as we know it - for the better. We scale impactful initiatives by conceptualizing, creating, and launching marketing campaigns that have touched hundreds of millions of lives in over 150 countries.

            Magic started with three Boulder-based marketers who wanted to use the power of capitalism to create a purpose-driven, lean digital agency accelerating missions that are of the highest stakes: the continuation and evolution of humankind… and sustaining the mother that feeds us.

            If you’re elevating consciousness, enhancing health and wellbeing, or regenerating the planet, we'll do whatever it takes to deliver results you're excited about.
          </h5>
        </GridItem>
      </GridContainer>
    </div>
  );
}

export default withStyles(descriptionStyle)(SectionDescription);
