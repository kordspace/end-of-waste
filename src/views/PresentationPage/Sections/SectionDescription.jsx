import React from "react";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import InfoArea from "components/InfoArea/InfoArea.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Button from "components/CustomButtons/Button.jsx";


// @material-ui icons
import Apps from "@material-ui/icons/Apps";
import ViewDay from "@material-ui/icons/ViewDay";
import ViewCarousel from "@material-ui/icons/ViewCarousel";
import Subject from "@material-ui/icons/Subject";
import WatchLater from "@material-ui/icons/WatchLater";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

import descriptionStyle from "assets/jss/material-kit-pro-react/views/presentationSections/descriptionStyle.jsx";

import office1 from "assets/img/examples/office1.jpg";
import cardBlog3 from "assets/img/examples/card-blog3.jpg";
import romania from "assets/img/romania.jpg";
import glass from "assets/img/glass.jpg";
import iot from "assets/img/iot.jpg";
import hivelogo from "assets/img/hive-logo.png";
import meetmindful from "assets/img/meetmindful.png";
import meetmindfullogo from "assets/img/meetmindful-logo.png";
import byron from "assets/img/byron.png";
import byronlogo from "assets/img/byron-logo.png";
import diamond from "assets/img/diamond.png";
import diamondlogo from "assets/img/diamond-logo.png";
import ned from "assets/img/ned.png";
import nedlogo from "assets/img/ned-logo.png";
import bulldog from "assets/img/bulldog.png";
import bulldoglogo from "assets/img/bulldog-logo.png";
import huckabuy from "assets/img/huckabuy.png";
import huckabuylogo from "assets/img/huckabuy-logo.png";
import superhero from "assets/img/superhero.png";
import superherologo from "assets/img/superhero-logo.png";

class SectionDescription extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem md={8} sm={8}>
              <h2 className={classes.description}>
              WHO ARE WE?
              </h2>
            </GridItem>
          </GridContainer>
              <div className={classes.container}>
                <div className={classes.description}>
                <h5>Everyone has the power to contribute to the shift; In fact we're all needed.</h5>
                <h5>We succeed through collective action, incentivizing a closed loop at all levels: from municipalities to multinationals.</h5>
                </div>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <Card
                      background
                      style={{ backgroundImage: `url(${romania})` }}
                    >
                      <CardBody background>
                        <h6 className={classes.cardCategoryWhite}></h6>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <h3 className={classes.cardTitleWhite}>
                            Based out of Romania
                          </h3>
                        </a>
                        <p className={classes.cardDescriptionWhite}>
                          <br />
                        </p>
                        <Button round color="primary">
                          <Subject /> Button
                        </Button>
                      </CardBody>
                    </Card>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Card
                      background
                      style={{ backgroundImage: `url(${glass})` }}
                    >
                      <CardBody background>
                        <h6 className={classes.cardCategoryWhite}></h6>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <h3 className={classes.cardTitleWhite}>
                            Recycling glass
                          </h3>
                        </a>
                        <p className={classes.cardDescriptionWhite}>
                          <br />
                        </p>
                        <Button round color="primary">
                          <Subject /> Button
                        </Button>
                      </CardBody>
                    </Card>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <Card
                      background
                      style={{ backgroundImage: `url(${iot})` }}
                    >
                      <CardBody background>
                        <h6 className={classes.cardCategoryWhite}></h6>
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <h3 className={classes.cardTitleWhite}>
                            IoT Tech Company
                          </h3>
                        </a>
                        <p className={classes.cardDescriptionWhite}>
                          <br />
                        </p>
                        <Button round color="primary">
                          <Subject /> Button
                        </Button>
                      </CardBody>
                    </Card>
                  </GridItem>
                </GridContainer>
              </div>
            </div>       
      </div>
    );
  }
}

export default withStyles(descriptionStyle)(SectionDescription);
