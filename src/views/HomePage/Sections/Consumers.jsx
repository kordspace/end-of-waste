/* eslint-disable prettier/prettier */
import React from "react";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";
import Share from "@material-ui/icons/Share";
import ChatBubble from "@material-ui/icons/ChatBubble";
import Schedule from "@material-ui/icons/Schedule";
import TrendingUp from "@material-ui/icons/TrendingUp";
import Subject from "@material-ui/icons/Subject";
import WatchLater from "@material-ui/icons/WatchLater";
import People from "@material-ui/icons/People";
import Business from "@material-ui/icons/Business";
import Check from "@material-ui/icons/Check";
import Close from "@material-ui/icons/Close";
import Delete from "@material-ui/icons/Delete";
import Bookmark from "@material-ui/icons/Bookmark";
import Refresh from "@material-ui/icons/Refresh";
import Receipt from "@material-ui/icons/Receipt";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import Info from "components/Typography/Info.jsx";
import Danger from "components/Typography/Danger.jsx";
import Success from "components/Typography/Success.jsx";
import Warning from "components/Typography/Warning.jsx";
import Rose from "components/Typography/Rose.jsx";
import Button from "components/CustomButtons/Button.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";

import LibraryBooks from "@material-ui/icons/LibraryBooks";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Slide from "@material-ui/core/Slide";

import styles from "assets/jss/material-kit-pro-react/views/homeSections/sectionConsumers.jsx";

import cardBlog1 from "assets/img/examples/card-blog1.jpg";
import cardBlog2 from "assets/img/examples/card-blog2.jpg";
import cardBlog3 from "assets/img/examples/card-blog3.jpg";
import cardBlog5 from "assets/img/examples/card-blog5.jpg";
import cardBlog6 from "assets/img/examples/card-blog6.jpg";
import cardProfile1 from "assets/img/examples/card-profile1.jpg";
import cardProfile4 from "assets/img/examples/card-profile4.jpg";
import blog1 from "assets/img/examples/blog1.jpg";
import blog5 from "assets/img/examples/blog5.jpg";
import blog6 from "assets/img/examples/blog6.jpg";
import blog8 from "assets/img/examples/blog8.jpg";
import avatar from "assets/img/faces/avatar.jpg";
import christian from "assets/img/faces/christian.jpg";
import marc from "assets/img/faces/marc.jpg";
import office1 from "assets/img/examples/office1.jpg";
import color1 from "assets/img/examples/color1.jpg";
import color2 from "assets/img/examples/color2.jpg";
import color3 from "assets/img/examples/color3.jpg";
import glassbg from "assets/img/about/glassbg.jpg";

function Transition(props) {
    return <Slide direction="down" {...props} />;
  }

class Consumers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeRotate1: "",
            activeRotate2: "",
            classicModal: false,
            classicModalt: false
        };
    }

    handleClickOpen(modal) {
        var x = [];
        x[modal] = true;
        this.setState(x);
      }
      handleClose(modal) {
        var x = [];
        x[modal] = false;
        this.setState(x);
      }
      handleClickOpen2(modalt) {
        var x = [];
        x[modalt] = true;
        this.setState(x);
      }
      handleClose2(modalt) {
        var x = [];
        x[modalt] = false;
        this.setState(x);
      }
      handleClosePopover(state) {
        this.setState({
          [state]: false
        });
      }
      handleClickButton(state) {
        this.setState({
          [state]: true
        });
      }

    componentDidMount() {
        const { classes } = this.props;
        var rotatingCards = document.getElementsByClassName(classes.cardRotate);
        for (let i = 0; i < rotatingCards.length; i++) {
            var rotatingCard = rotatingCards[i];
            var rotatingWrapper = rotatingCard.parentElement;
            var cardWidth = rotatingCard.parentElement.offsetWidth;
            var cardHeight = rotatingCard.children[0].children[0].offsetHeight;
            rotatingWrapper.style.height = cardHeight + "px";
            rotatingWrapper.style["margin-bottom"] = 30 + "px";
            var cardFront = rotatingCard.getElementsByClassName(classes.front)[0];
            var cardBack = rotatingCard.getElementsByClassName(classes.back)[0];
            cardFront.style.height = cardHeight + 35 + "px";
            cardFront.style.width = cardWidth + "px";
            cardBack.style.height = cardHeight + 35 + "px";
            cardBack.style.width = cardWidth + "px";
        }
    }
    render() {
        const { classes, ...rest } = this.props;
        const { consumers } = this.props;
        if (consumers) {
            return (
                <div {...rest} className="cd-section" id="consumers">
                    <div className={classes.sectionGray}>
                        <div className={classes.containerImage}
                            style={{
                                background: `linear-gradient( rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) ), url(${consumers[0].imagelink})`
                            }}>
                            {/* DYNAMIC COLORED SHADOWS START */}
                            <div className={classes.container}>
                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={12}>
                                        <div className={classes.title}>
                                            <h2 className="center">{consumers[0].content}</h2>
                                            <h3 className="center">{consumers[1].content}</h3>
                                            <h4 className="center">{consumers[2].content}</h4>
                                            <br />
                                        </div>
                                    </GridItem>
                                </GridContainer>
                                <div className={classes.consumersContainer}>
                                    <GridContainer>
                                        <GridItem xs={12} sm={12} md={6}>
                                            <Card blog plain className={classes.cardColor}>
                                                <CardHeader image plain>
                                                    <img src={consumers[3].imagelink} alt="waste bin in a city" />
                                                    <div
                                                        className={classes.coloredShadow}
                                                        style={{
                                                            backgroundImage: `url(${consumers[3].imagelink})`,
                                                            opacity: "1"
                                                        }}
                                                    />
                                                </CardHeader>
                                                <CardBody plain className={classes.consumersCardBody}>
                                                    {/*<Warning>
                                                    <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                                                    </Warning>
                                                    <h4 className={classes.aboutFeatureDescription}>
                                                        <i>{consumers[3].content}</i>
                                                    </h4>*/}
                                                </CardBody>
                                            </Card>
                                        </GridItem>
                                        <GridItem xs={12} sm={12} md={6}>
                                            <Card blog plain className={classes.cardColor}>
                                                <CardHeader image>
                                                    <a href="#pablo" onClick={e => e.preventDefault()}>
                                                    </a>
                                                    <div
                                                    />
                                                </CardHeader>
                                                <CardBody plain className={classes.aboutSignUp}>
                                                    {/*<Rose>
                                    <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                                    </Rose>*/}
                                                    <h3 className={classes.aboutSignUpTitle}>
                                                        {consumers[4].content}
                                                    </h3>
                                                    <h4 className={classes.aboutSignUpDescription}>
                                                        {consumers[5].content}
                                                    </h4><br></br>
                                                    <div className={classes.consumerForms}>
                                                        {/*<GridContainer>
                                                            <GridItem xs={12} sm={12} md={12}>
                                                                <CustomInput
                                                                    id="not-logged-name"
                                                                    formControlProps={{
                                                                        fullWidth: true
                                                                    }}
                                                                    inputProps={{
                                                                        placeholder: "Your Name"
                                                                    }}
                                                                />
                                                            </GridItem>
                                                            <GridItem xs={12} sm={12} md={12}>
                                                                <CustomInput
                                                                    id="not-logged-email"
                                                                    formControlProps={{
                                                                        fullWidth: true
                                                                    }}
                                                                    inputProps={{
                                                                        placeholder: "Your Email"
                                                                    }}
                                                                />
                                                            </GridItem>
                                                        </GridContainer>
                                                        <Button
                                                            color="info"
                                                            className={classes.navButton}
                                                            round
                                                        >
                                                            {consumers[6].content}
                                                                </Button>*/}
                                                    </div>

                                                    <GridContainer>
                                <GridItem className={classes.topsy} xs={12} sm={12} md={12} lg={12}>
                                                        <Button className={classes.carouselNavButton}
                                                            color="info"
                                                            block
                                                            round
                                                            onClick={() => this.handleClickOpen("classicModal")}
                                                        >
                                                            <LibraryBooks />{consumers[6].content}
                                                        </Button>
                                                        <Dialog
                                                            classes={{
                                                            root: classes.modalRoot,
                                                            paper: classes.modal
                                                            }}
                                                            open={this.state.classicModal}
                                                            TransitionComponent={Transition}
                                                            keepMounted
                                                            onClose={() => this.handleClose("classicModal")}
                                                            aria-labelledby="classic-modal-slide-title"
                                                            aria-describedby="classic-modal-slide-description"
                                                        >
                                                            <DialogTitle
                                                            id="classic-modal-slide-title"
                                                            disableTypography
                                                            className={classes.modalHeader}
                                                            >
                                                            <Button className={classes.rightLife}
                                                                onClick={() => this.handleClose("classicModal")}
                                                                color="danger"
                                                                simple
                                                            >
                                                                Close
                                                            </Button>
                                                            <Button
                                                                simple
                                                                className={classes.modalCloseButton}
                                                                key="close"
                                                                aria-label="Close"
                                                                onClick={() => this.handleClose("classicModal")}
                                                            >
                                                                {" "}
                                                                <Close className={classes.modalClose} />
                                                            </Button>
                                                            <div>
                                                            <h2 className={classes.modalTitle}><center>End Of Waste Pilot Program Waitlist</center></h2>
                                                            </div>
                                                            </DialogTitle>
                                                            <DialogContent
                                                            id="classic-modal-slide-description"
                                                            className={classes.modalBody}
                                                            >
               
                                                            
  {/* Begin Mailchimp Signup Form */}
  <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css" />
  <style type="text/css" dangerouslySetInnerHTML={{__html: "\n\t#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }\n\t/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.\n\t   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */\n" }} />
  <div id="mc_embed_signup">
    <form action="https://endofwaste.us19.list-manage.com/subscribe/post?u=ba44c2bfd89ab186b45e29b54&id=5ccc9cf9d0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate>
      <div id="mc_embed_signup_scroll">
        <div className="indicates-required"><span className="asterisk">*</span> indicates required</div>
        <div className="mc-field-group">
          <label htmlFor="mce-EMAIL">Email Address  <span className="asterisk">*</span>
          </label>
          <input type="email" name="EMAIL" className="required email" id="mce-EMAIL" />
        </div>
        <div className="mc-field-group">
          <label htmlFor="mce-FNAME">First Name </label>
          <input type="text" name="FNAME" className id="mce-FNAME" />
        </div>
        <div className="mc-field-group">
          <label htmlFor="mce-LNAME">Last Name </label>
          <input type="text" name="LNAME" className id="mce-LNAME" />
        </div>
        <div id="mce-responses" className="clear">
          <div className="response" id="mce-error-response" style={{display: 'none'}} />
          <div className="response" id="mce-success-response" style={{display: 'none'}} />
        </div>    {/* real people should not fill this in and expect good things - do not remove this or risk form bot signups*/}
        <div style={{position: 'absolute', left: '-5000px'}} aria-hidden="true"><input type="text" name="b_ba44c2bfd89ab186b45e29b54_5ccc9cf9d0" tabIndex={-1} defaultValue /></div>
        <div className="clear"><input type="submit" defaultValue="Subscribe" name="subscribe" id="mc-embedded-subscribe" className="button" /></div>
      </div>
    </form>
  </div>
  {/*End mc_embed_signup*/}
                                                       


                                                            </DialogContent>
                                                            <DialogActions className={classes.modalFooter}>
                                                            </DialogActions>
                                                        </Dialog>
                                                    </GridItem>                                    
                                </GridContainer>

                                                </CardBody>
                                            </Card>
                                        </GridItem>
                                    </GridContainer>
                                    <GridContainer>
                                    <GridItem xs={12} sm={12} md={6}>
                                            <Card blog plain className={classes.cardColor}>
                                                <CardHeader image>
                                                    <a href="#pablo" onClick={e => e.preventDefault()}>
                                                    </a>
                                                    <div
                                                    />
                                                </CardHeader>
                                                <CardBody plain className={classes.aboutSignUp}>
                                                    {/*<Rose>
                                    <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                                    </Rose>*/}
                                                    <h3 className={classes.aboutSignUpTitle}>
                                                        {consumers[8].content}
                                                    </h3>
                                                    <h4 className={classes.aboutSignUpDescription}>
                                                        {consumers[9].content}
                                                    </h4><br></br>
                                                    <div className={classes.consumerForms}>
                                                        {/*<GridContainer>
                                                            <GridItem xs={12} sm={12} md={12}>
                                                                <CustomInput
                                                                    id="not-logged-name"
                                                                    formControlProps={{
                                                                        fullWidth: true
                                                                    }}
                                                                    inputProps={{
                                                                        placeholder: "Your Name"
                                                                    }}
                                                                />
                                                            </GridItem>
                                                            <GridItem xs={12} sm={12} md={12}>
                                                                <CustomInput
                                                                    id="not-logged-email"
                                                                    formControlProps={{
                                                                        fullWidth: true
                                                                    }}
                                                                    inputProps={{
                                                                        placeholder: "Your Email"
                                                                    }}
                                                                />
                                                            </GridItem>
                                                        </GridContainer>
                                                        <Button
                                                            color="info"
                                                            className={classes.navButton}
                                                            round
                                                        >
                                                            {consumers[6].content}
                                                                </Button>*/}
                                                    </div>
                                                    
                                                    <GridContainer>
                                <GridItem className={classes.topsy} xs={12} sm={12} md={12} lg={12}>
                                                        <Button className={classes.carouselNavButton}
                                                            color="info"
                                                            block
                                                            round
                                                            onClick={() => this.handleClickOpen2("classicModalt")}
                                                        >
                                                            <LibraryBooks />{consumers[10].content}
                                                        </Button>
                                                        <Dialog
                                                            classes={{
                                                            root: classes.modalRoot,
                                                            paper: classes.modal
                                                            }}
                                                            open={this.state.classicModalt}
                                                            TransitionComponent={Transition}
                                                            keepMounted
                                                            onClose={() => this.handleClose2("classicModalt")}
                                                            aria-labelledby="classic-modal-slide-title"
                                                            aria-describedby="classic-modal-slide-description"
                                                        >
                                                            <DialogTitle
                                                            id="classic-modal-slide-title"
                                                            disableTypography
                                                            className={classes.modalHeader}
                                                            >
                                                            <Button className={classes.rightLife}
                                                                onClick={() => this.handleClose2("classicModalt")}
                                                                color="danger"
                                                                simple
                                                            >
                                                                Close
                                                            </Button>
                                                            <Button
                                                                simple
                                                                className={classes.modalCloseButton}
                                                                key="close"
                                                                aria-label="Close"
                                                                onClick={() => this.handleClose2("classicModalt")}
                                                            >
                                                                {" "}
                                                                <Close className={classes.modalClose} />
                                                            </Button>
                                                            <div>
                                                            <h2 className={classes.modalTitle}><center>End Of Waste Pilot Program Waitlist</center></h2>
                                                            </div>
                                                            </DialogTitle>
                                                            <DialogContent
                                                            id="classic-modal-slide-description"
                                                            className={classes.modalBody}
                                                            >
                                                            
                                                            
                                                            {/* Begin Mailchimp Signup Form */}
                                                            <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css" />
                                                            <style type="text/css" dangerouslySetInnerHTML={{__html: "\n\t#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }\n\t/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.\n\t   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */\n" }} />
                                                            <div id="mc_embed_signup">
                                                                <form action="https://endofwaste.us19.list-manage.com/subscribe/post?u=ba44c2bfd89ab186b45e29b54&id=f0c7addd60" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate>
                                                                <div id="mc_embed_signup_scroll">
                                                                    <div className="indicates-required"><span className="asterisk">*</span> indicates required</div>
                                                                    <div className="mc-field-group">
                                                                    <label htmlFor="mce-EMAIL">Email Address  <span className="asterisk">*</span>
                                                                    </label>
                                                                    <input type="email" name="EMAIL" className="required email" id="mce-EMAIL" />
                                                                    </div>
                                                                    <div className="mc-field-group">
                                                                    <label htmlFor="mce-FNAME">First Name </label>
                                                                    <input type="text" name="FNAME" className id="mce-FNAME" />
                                                                    </div>
                                                                    <div className="mc-field-group">
                                                                    <label htmlFor="mce-LNAME">Last Name </label>
                                                                    <input type="text" name="LNAME" className id="mce-LNAME" />
                                                                    </div>
                                                                    <div id="mce-responses" className="clear">
                                                                    <div className="response" id="mce-error-response" style={{display: 'none'}} />
                                                                    <div className="response" id="mce-success-response" style={{display: 'none'}} />
                                                                    </div>    {/* real people should not fill this in and expect good things - do not remove this or risk form bot signups*/}
                                                                    <div style={{position: 'absolute', left: '-5000px'}} aria-hidden="true"><input type="text" name="b_ba44c2bfd89ab186b45e29b54_f0c7addd60" tabIndex={-1} defaultValue /></div>
                                                                    <div className="clear"><input type="submit" defaultValue="Subscribe" name="subscribe" id="mc-embedded-subscribe" className="button" /></div>
                                                                </div>
                                                                </form>
                                                            </div>
                                                            {/*End mc_embed_signup*/}
                                                            
                                                            </DialogContent>
                                                            <DialogActions className={classes.modalFooter}>
                                                            </DialogActions>
                                                        </Dialog>
                                                    </GridItem>                                    
                                </GridContainer>

                                                </CardBody>
                                            </Card>
                                        </GridItem>
                                        <GridItem xs={12} sm={12} md={6}>
                                            <Card blog plain className={classes.cardColor}>
                                                <CardHeader image plain>
                                                    <img src={consumers[7].imagelink} alt="waste bin in a city" />
                                                    <div
                                                        className={classes.coloredShadow}
                                                        style={{
                                                            backgroundImage: `url(${consumers[7].imagelink})`,
                                                            opacity: "1"
                                                        }}
                                                    />
                                                </CardHeader>
                                                <CardBody plain className={classes.consumersCardBody}>
                                                    {/*<Warning>
                                                    <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                                                    </Warning>
                                                    <h4 className={classes.aboutFeatureDescription}>
                                                        <i>{consumers[3].content}</i>
                                                    </h4>*/}
                                                </CardBody>
                                            </Card>
                                        </GridItem>
                                        
                                    </GridContainer>
                                    {/*<GridContainer>
                                        <GridItem xs={12} sm={12} md={8}>
                                            <Card blog className={classes.cardColor}>
                                                <CardHeader image>
                                                    <a href="#pablo" onClick={e => e.preventDefault()}>
                                                    </a>
                                                    <div
                                                    />
                                                </CardHeader>
                                                <CardBody className={classes.aboutSignUp}>
                                                   
                                                    <h3 className={classes.aboutSignUpTitle}>
                                                        {consumers[8].content}
                                                    </h3>
                                                    <h5 className={classes.aboutSignUpDescription}>
                                                        {consumers[9].content}
                                                    </h5><br></br>
                                                    <div className={classes.consumerForms}>
                                                        {/*<GridContainer>
                                                            <GridItem xs={12} sm={12} md={12}>
                                                                <CustomInput
                                                                    id="not-logged-name"
                                                                    formControlProps={{
                                                                        fullWidth: true
                                                                    }}
                                                                    inputProps={{
                                                                        placeholder: "Your Name"
                                                                    }}
                                                                />
                                                            </GridItem>
                                                            <GridItem xs={12} sm={12} md={12}>
                                                                <CustomInput
                                                                    id="not-logged-email"
                                                                    formControlProps={{
                                                                        fullWidth: true
                                                                    }}
                                                                    inputProps={{
                                                                        placeholder: "Your Email"
                                                                    }}
                                                                />
                                                            </GridItem>
                                                        </GridContainer>
                                                        <Button
                                                            color="info"
                                                            className={classes.navButton}
                                                            round
                                                        >
                                                            {consumers[6].content}
                                                                </Button>
                                                    </div>
                                                </CardBody>
                                            </Card>
                                        </GridItem>
                                        <GridItem xs={12} sm={12} md={4}>
                                            <Card blog className={classes.cardColor}>
                                                <CardHeader image>
                                                    <img src={consumers[7].imagelink} alt="waste bin in a city" />
                                                    <div
                                                        className={classes.coloredShadow}
                                                        style={{
                                                            backgroundImage: `url(${consumers[7].imagelink})`,
                                                            opacity: "1"
                                                        }}
                                                    />
                                                </CardHeader>
                                                <CardBody className={classes.consumersCardBody}>
                                                    {/*<Warning>
                                                    <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                                                    </Warning>
                                                    <h4 className={classes.aboutFeatureDescription}>
                                                        <i>{consumers[7].content}</i>
                                                    </h4>
                                                </CardBody>
                                            </Card>
                                        </GridItem>
                                    </GridContainer>*/}
                                </div>
                            </div>
                            {/* DYNAMIC COLORED SHADOWS END */}
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div></div>
            )
        }
    }
}

export default withStyles(styles)(Consumers);
