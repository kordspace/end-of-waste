import React from "react";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";
import Share from "@material-ui/icons/Share";
import ChatBubble from "@material-ui/icons/ChatBubble";
import Schedule from "@material-ui/icons/Schedule";
import TrendingUp from "@material-ui/icons/TrendingUp";
import Subject from "@material-ui/icons/Subject";
import WatchLater from "@material-ui/icons/WatchLater";
import People from "@material-ui/icons/People";
import Business from "@material-ui/icons/Business";
import Check from "@material-ui/icons/Check";
import Close from "@material-ui/icons/Close";
import Delete from "@material-ui/icons/Delete";
import Bookmark from "@material-ui/icons/Bookmark";
import Refresh from "@material-ui/icons/Refresh";
import Receipt from "@material-ui/icons/Receipt";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import Info from "components/Typography/Info.jsx";
import Danger from "components/Typography/Danger.jsx";
import Success from "components/Typography/Success.jsx";
import Warning from "components/Typography/Warning.jsx";
import Rose from "components/Typography/Rose.jsx";
import Button from "components/CustomButtons/Button.jsx";

import styles from "assets/jss/material-kit-pro-react/views/homeSections/sectionSocial.jsx";

import cardBlog1 from "assets/img/examples/card-blog1.jpg";
import cardBlog2 from "assets/img/examples/card-blog2.jpg";
import cardBlog3 from "assets/img/examples/card-blog3.jpg";
import cardBlog5 from "assets/img/examples/card-blog5.jpg";
import cardBlog6 from "assets/img/examples/card-blog6.jpg";
import cardProfile1 from "assets/img/examples/card-profile1.jpg";
import cardProfile4 from "assets/img/examples/card-profile4.jpg";
import blog1 from "assets/img/examples/blog1.jpg";
import blog5 from "assets/img/examples/blog5.jpg";
import blog6 from "assets/img/examples/blog6.jpg";
import blog8 from "assets/img/examples/blog8.jpg";
import avatar from "assets/img/faces/avatar.jpg";
import christian from "assets/img/faces/christian.jpg";
import marc from "assets/img/faces/marc.jpg";
import office1 from "assets/img/examples/office1.jpg";
import color1 from "assets/img/examples/color1.jpg";
import color2 from "assets/img/examples/color2.jpg";
import color3 from "assets/img/examples/color3.jpg";
import card1 from "assets/img/about/card1.jpg";
import card2 from "assets/img/about/card2.jpg";
import card3 from "assets/img/about/card3.jpg";
import instagram from "assets/img/instagram.jpg";
import facebook from "assets/img/facebook.jpg";
import linkedin from "assets/img/linkedin.jpg";

import classNames from "classnames";

import face1 from "assets/img/social/face1.jpg";
import face2 from "assets/img/social/face2.jpg";
import face4 from "assets/img/social/avatar.png";



class Social extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeRotate1: "",
            activeRotate2: ""
        };
    }
    componentDidMount() {
        const { classes } = this.props;
        var rotatingCards = document.getElementsByClassName(classes.cardRotate);
        for (let i = 0; i < rotatingCards.length; i++) {
            var rotatingCard = rotatingCards[i];
            var rotatingWrapper = rotatingCard.parentElement;
            var cardWidth = rotatingCard.parentElement.offsetWidth;
            var cardHeight = rotatingCard.children[0].children[0].offsetHeight;
            rotatingWrapper.style.height = cardHeight + "px";
            rotatingWrapper.style["margin-bottom"] = 30 + "px";
            var cardFront = rotatingCard.getElementsByClassName(classes.front)[0];
            var cardBack = rotatingCard.getElementsByClassName(classes.back)[0];
            cardFront.style.height = cardHeight + 35 + "px";
            cardFront.style.width = cardWidth + "px";
            cardBack.style.height = cardHeight + 35 + "px";
            cardBack.style.width = cardWidth + "px";
        }
    }
    render() {
        const { classes, ...rest } = this.props;
        const { origins } = this.props

        if (origins) {
            return (
                <div {...rest} className="cd-section" id="social">
                    <div className={classes.sectionGray}>
                        <div className={classes.containerImage}
                            style={{
                                background: `white`
                            }}>
                            {/* DYNAMIC COLORED SHADOWS START */}
                            <div className={classes.container}>

                                <GridContainer>
                                    <GridItem xs={12} sm={12} md={12}>
                                        <div className={classes.headerTitle}>
                                            <h2 className="center">{origins[0].content}</h2>
                                            <h3 className="center">{origins[1].content}</h3>
                                            <h5 className="center">{origins[2].content}</h5>
                                        </div>
                                    </GridItem>
                                </GridContainer>
                                <GridContainer className={classes.socialGridContainer}>
                                    <GridItem md={4} sm={12}>
                                        <Card profile plain className={classes.cardColor}>
                                            <div className={classes.cardAvatar}>
                                                <CardAvatar profile plain >
                                                    <img
                                                        src={origins[3].image_link}
                                                        alt="profile picture of Ionut Georgescu"
                                                        className={classes.img}
                                                    />
                                                </CardAvatar>
                                            </div>
                                            <CardBody plain>
                                                <h4 className={classes.avatarTitle}> {origins[3].content}</h4>
                                                <h5 className={classes.avatarSubTitle}> {origins[4].content}</h5>
                                                <p className={classes.socialTeamDescriptionOana}>
                                                    {origins[5].content}
                                                </p><br></br>
                                            </CardBody>
                                            {/*<CardFooter className={classes.justifyContent}>
                        <Button href="#pablo" justIcon simple color="twitter">
                            <i className="fab fa-twitter" />
                        </Button>
                        <Button href="#pablo" justIcon simple color="dribbble">
                            <i className="fab fa-dribbble" />
                        </Button>
                        <Button href="#pablo" justIcon simple color="linkedin">
                            <i className="fab fa-linkedin-in" />
                        </Button>
                        </CardFooter>*/}
                                        </Card>
                                    </GridItem>
                                    <GridItem md={4} sm={12}>
                                        <Card profile plain className={classes.cardColor}>
                                            <div className={classes.cardAvatar}>
                                                <CardAvatar profile plain >
                                                    <img
                                                        src={origins[6].image_link}
                                                        alt="profile picture of Oana Molodoi"
                                                        className={classes.img}
                                                    />
                                                </CardAvatar>
                                            </div>
                                            <CardBody plain>
                                                <h4 className={classes.avatarTitle}>{origins[6].content}</h4>
                                                <h5 className={classes.avatarSubTitle}>{origins[7].content}</h5>
                                                <p className={classes.socialTeamDescriptionOana}>
                                                    {origins[8].content}
                                                </p>
                                            </CardBody>
                                            {/*<CardFooter className={classes.justifyContent}>
                        <Button href="#pablo" justIcon simple color="facebook">
                            <i className="fab fa-facebook" />
                        </Button>
                        <Button href="#pablo" justIcon simple color="dribbble">
                            <i className="fab fa-dribbble" />
                        </Button>
                        </CardFooter>*/}
                                        </Card>
                                    </GridItem>
                                    <GridItem md={4} sm={12}>
                                        <Card profile plain className={classes.cardColor}>
                                            <div className={classes.cardAvatar}>
                                                <CardAvatar profile plain>
                                                    <img
                                                        src={origins[9].image_link}
                                                        alt="profile picture of Gordon Starr"
                                                        className={classes.img}
                                                    />
                                                </CardAvatar>
                                            </div>
                                            <CardBody plain>
                                                <h4 className={classes.avatarTitle}>{origins[9].content}</h4>
                                                <h5 className={classes.avatarSubTitle}>{origins[10].content}</h5>
                                                <p className={classes.socialTeamDescriptionOana}>
                                                    {origins[11].content}
                                                </p>
                                            </CardBody>
                                            {/*<CardFooter className={classes.justifyContent}>
                        <Button href="#pablo" justIcon simple color="google">
                            <i className="fab fa-google" />
                        </Button>
                        <Button href="#pablo" justIcon simple color="twitter">
                            <i className="fab fa-twitter" />
                        </Button>
                        <Button href="#pablo" justIcon simple color="dribbble">
                            <i className="fab fa-dribbble" />
                        </Button>
                        </CardFooter>*/}
                                        </Card>
                                    </GridItem>
                                </GridContainer>
                                <GridContainer className={classes.socialGridContainer}>
                                    <GridItem md={4} sm={12}>
                                        <Card profile plain className={classes.cardColor}>
                                            <div className={classes.cardAvatar}>
                                                <CardAvatar profile plain >
                                                    <img
                                                        src={origins[12].image_link}
                                                        alt="profile picture of Ionut Georgescu"
                                                        className={classes.img}
                                                    />
                                                </CardAvatar>
                                            </div>
                                            <CardBody plain>
                                                <h4 className={classes.avatarTitle}> {origins[12].content}</h4>
                                                <h5 className={classes.avatarSubTitle}> {origins[13].content}</h5>
                                                <p className={classes.socialTeamDescriptionOana}>
                                                    {origins[14].content}
                                                </p><br></br>
                                            </CardBody>
                                            {/*<CardFooter className={classes.justifyContent}>
                        <Button href="#pablo" justIcon simple color="twitter">
                            <i className="fab fa-twitter" />
                        </Button>
                        <Button href="#pablo" justIcon simple color="dribbble">
                            <i className="fab fa-dribbble" />
                        </Button>
                        <Button href="#pablo" justIcon simple color="linkedin">
                            <i className="fab fa-linkedin-in" />
                        </Button>
                        </CardFooter>*/}
                                        </Card>
                                    </GridItem>
                                    <GridItem md={4} sm={12}>
                                        <Card profile plain className={classes.cardColor}>
                                            <div className={classes.cardAvatar}>
                                                <CardAvatar profile plain >
                                                    <img
                                                        src={origins[15].image_link}
                                                        alt="profile picture of Oana Molodoi"
                                                        className={classes.img}
                                                    />
                                                </CardAvatar>
                                            </div>
                                            <CardBody plain>
                                                <h4 className={classes.avatarTitle}>{origins[15].content}</h4>
                                                <h5 className={classes.avatarSubTitle}>{origins[16].content}</h5>
                                                <p className={classes.socialTeamDescriptionOana}>
                                                    {origins[17].content}
                                                </p>
                                            </CardBody>
                                            {/*<CardFooter className={classes.justifyContent}>
                        <Button href="#pablo" justIcon simple color="facebook">
                            <i className="fab fa-facebook" />
                        </Button>
                        <Button href="#pablo" justIcon simple color="dribbble">
                            <i className="fab fa-dribbble" />
                        </Button>
                        </CardFooter>*/}
                                        </Card>
                                    </GridItem>
                                    <GridItem md={4} sm={12}>
                                        <Card profile plain className={classes.cardColor}>
                                            <div className={classes.cardAvatar}>
                                                <CardAvatar profile plain>
                                                    <img
                                                        src={origins[18].image_link}
                                                        alt="profile picture of Gordon Starr"
                                                        className={classes.img}
                                                    />
                                                </CardAvatar>
                                            </div>
                                            <CardBody plain>
                                                <h4 className={classes.avatarTitle}>{origins[18].content}</h4>
                                                <h5 className={classes.avatarSubTitle}>{origins[19].content}</h5>
                                                <p className={classes.socialTeamDescriptionOana}>
                                                    {origins[20].content}
                                                </p>
                                            </CardBody>
                                            {/*<CardFooter className={classes.justifyContent}>
                        <Button href="#pablo" justIcon simple color="google">
                            <i className="fab fa-google" />
                        </Button>
                        <Button href="#pablo" justIcon simple color="twitter">
                            <i className="fab fa-twitter" />
                        </Button>
                        <Button href="#pablo" justIcon simple color="dribbble">
                            <i className="fab fa-dribbble" />
                        </Button>
                        </CardFooter>*/}
                                        </Card>
                                    </GridItem>
                                </GridContainer>
                            </div>
                            {/* DYNAMIC COLORED SHADOWS END */}
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div></div>
            )
        }
    }
}

export default withStyles(styles)(Social);
