import React from "react";
import { compose } from "redux"
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Redirect } from 'react-router-dom'
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";
import Arrow from "@material-ui/icons/ArrowDownward";

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Button from "components/CustomButtons/Button.jsx";

// sections for this page
import About from "./Sections/About.jsx";
import Consumers from "./Sections/Consumers.jsx";
import Suppliers from "./Sections/Suppliers1.jsx";
import Social from "./Sections/Social.jsx";
import Form from "./Sections/Form.jsx";

import SectionBasics from "./Sections/SectionBasics.jsx";
import SectionNavbars from "./Sections/SectionNavbars.jsx";
import SectionTabs from "./Sections/SectionTabs.jsx";
import SectionPills from "./Sections/SectionPills.jsx";
import SectionPreFooter from "./Sections/SectionPreFooter.jsx";
import SectionFooter from "./Sections/SectionFooter.jsx";
import SectionTypography from "./Sections/SectionTypography.jsx";
import SectionCards from "./Sections/SectionCards.jsx";
import SectionJavascript from "./Sections/SectionJavascript.jsx";
import SectionCarousel from "./Sections/SectionCarousel.jsx";
import Carousel from "react-slick";
import LocationOn from "@material-ui/icons/LocationOn";
import image1 from "assets/img/bg.jpg";
import image2 from "assets/img/bg2.jpg";
import image3 from "assets/img/bg3.jpg";

import homeStyle from "assets/jss/material-kit-pro-react/views/homeStyle.jsx";

import logo from "../../assets/img/eow_herologo.png";
import CardFooter from "components/Card/CardFooter.jsx";

import HomePageFooter from "./HomePageFooter";
import HomePageFooterCenter from "./HomePageFooterCenter";
import Splash from "./Sections/Splash";
class Components extends React.Component {
  componentDidMount() {
    var href = window.location.href.substring(
      window.location.href.lastIndexOf("#") + 1
    );
    if (window.location.href.lastIndexOf("#") > 0)
      document.getElementById(href).scrollIntoView();
    window.addEventListener("scroll", this.updateView);
    this.updateView();
  }
  componentDidUpdate() {
    var href = window.location.href.substring(
      window.location.href.lastIndexOf("#") + 1
    );
    if (href.includes("/")) {
      document.getElementById("home").scrollIntoView();
    } else {
      document.getElementById(href).scrollIntoView();
    }
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.updateView);
  }
  easeInOutQuad(t, b, c, d) {
    t /= d / 2;
    if (t < 1) return c / 2 * t * t + b;
    t--;
    return -c / 2 * (t * (t - 2) - 1) + b;
  }
  updateView() {
    var contentSections = document.getElementsByClassName("cd-section");
    var navigationItems = document
      .getElementById("cd-vertical-nav")
      .getElementsByTagName("a");

      for (let i = 0; i < contentSections.length; i++) {
        var activeSection =
          parseInt(navigationItems[i].getAttribute("data-number"), 10) - 1;
        if (
          contentSections[i].offsetTop -
          window.innerHeight / 2 +
          document.getElementById("main-panel").offsetTop <
          window.pageYOffset &&
          contentSections[i].offsetTop +
          contentSections[i].scrollHeight -
          window.innerHeight / 2 +
          document.getElementById("main-panel").offsetTop >
          window.pageYOffset
        ) {
          navigationItems[activeSection].classList.add("is-selected");
        } else {
          navigationItems[activeSection].classList.remove("is-selected");
        }
      }
    }
    smoothScroll(target) {
      var targetScroll = document.getElementById(target);
      this.scrollTo(document.documentElement, targetScroll.offsetTop, 900);
    }
    scrollTo(element, to, duration) {
      var start = element.scrollTop,
        change = to - start + document.getElementById("main-panel").offsetTop,
        currentTime = 0,
        increment = 20;
  
      var animateScroll = function () {
        currentTime += increment;
        var val = this.easeInOutQuad(currentTime, start, change, duration);
        element.scrollTop = val;
        if (currentTime < duration) {
          setTimeout(animateScroll, increment);
        }
      }.bind(this);
      animateScroll();
    }
    render() {
    const { classes } = this.props;
    const { hero } = this.props;
    const { about } = this.props;
    const { consumers } = this.props;
    const { suppliers } = this.props;
    const { origins } = this.props;
    const { contact } = this.props;
    const { footer } = this.props;
    const { navbar } = this.props;
    const settings = {
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplaySpeed: 7000,
      autoplay: true
    };

    console.log(this.props);
    if (hero) {
      return (
        <div id="home">
          <Header
            brand="End Of Waste React"
            links={<HeaderLinks dropdownHoverColor="info" navbar={navbar} />}
            fixed
            color="transparent"
            changeColorOnScroll={{
              height: 400,
              color: "info"
            }}
          />
          <div id="carousel">
            <div>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <Carousel {...settings} className={classes.carouselHero}>
                    <div className={classes.carouselContent}>
                      <img
                        src={hero[0].image_link}
                        alt="First slide"
                        className="slick-image"
                      />
                      <h1>{hero[0].content}</h1>
                      <h2>{hero[1].content}</h2><br></br>
                      <Button
                        onClick={e => {
                          e.preventDefault();
                          this.smoothScroll("about");
                        }}
                        color="info"
                        className={classes.carouselNavButton}
                        round
                      >
                        {hero[2].content}
                      </Button>

                      {/*<div className="slick-caption">
                      <h4>
                        <LocationOn className="slick-icons" />Yellowstone
                        National Park, United States
                      </h4>
    </div>*/}
                    </div>
                    <div className={classes.carouselContent}>
                      <img
                        src={hero[3].image_link}
                        alt="First slide"
                        className="slick-image"
                      />
                      <h1>{hero[3].content}</h1>
                      <h2>{hero[4].content}</h2><br></br><br></br>
                      <Button
                        onClick={e => {
                          e.preventDefault();
                          this.smoothScroll("consumers");
                        }}
                        color="info"
                        className={classes.carouselNavButton}
                        round
                      >
                        {hero[5].content}
                      </Button>

                      {/*<div className="slick-caption">
                      <h4>
                        <LocationOn className="slick-icons" />Yellowstone
                        National Park, United States
                      </h4>
    </div>*/}
                    </div>
                    <div className={classes.carouselContent}>
                      <img
                        src={hero[6].image_link}
                        alt="First slide"
                        className="slick-image"
                      />
                      <h1>{hero[6].content}</h1>
                      <h2>{hero[7].content}</h2><br></br>
                      <Button
                        onClick={e => {
                          e.preventDefault();
                          this.smoothScroll("suppliers");
                        }}
                        color="info"
                        className={classes.carouselNavButton}
                        round
                      >
                        {hero[8].content}
                      </Button>

                      {/*<div className="slick-caption">
                      <h4>
                        <LocationOn className="slick-icons" />Yellowstone
                        National Park, United States
                      </h4>
    </div>*/}
                    </div>
                  </Carousel>
                </GridItem>
              </GridContainer>
            </div>
          </div>

          {/*<SectionCarousel />
        <Parallax
          image={require("assets/img/bg18-min.jpg")}
          className={classes.parallax}
        >
          <div className={classes.container}><br></br><br></br>
            <GridContainer>
              <GridItem xs={12} sm={12} md={7}>
                <div className={classes.brand}>
                  <h1>{hero[0].content}
                  </h1>
                  <h3>
                    {hero[1].content}
                  </h3>
                </div>
              </GridItem>                  
              <GridItem xs={12} sm={12} md={5}><br></br>
                <div className={classes.brand}>
                <h2>
                     <i>{hero[2].content}</i>
                </h2>
                  <div>
                    <Button
                        onClick={e => {
                          e.preventDefault();
                          this.smoothScroll("about");
                        }}
                        color="info"
                        className={classes.navButton}
                        round
                      >
                        {hero[3].content}
                      </Button>
                    </div>
                </div>
              </GridItem>  
                      </GridContainer>
          </div>
        </Parallax>*/}

          <div
            className={classNames(classes.main, classes.mainRaised)}
            id="main-panel"
          >
            <div className={classes.about}>
              <About about={about} />
            </div>
            <Consumers consumers={consumers} />
            <Suppliers suppliers={suppliers} />
            <Social origins={origins} />
            <Form contact={contact} />
            {/*<SectionBasics />
          <SectionNavbars />
          <SectionTabs />*/}
            {/*<div
            className={classNames(
              classes.section,
              classes.sectionGray,
              "cd-section"
            )}
            id="footers"
          >
            <SectionPreFooter />
            <SectionFooter />
            </div>
          <SectionTypography />
          <SectionCards />
          <SectionJavascript />*/}

          </div>
          <nav id="cd-vertical-nav">
          <ul>
            <li>
              <a
                href="#home"
                data-number="1"
                className="is-selected"
                //onClick={e => {
                 // var isMobile = navigator.userAgent.match(
                 //   /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                 // );
                 // if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                 // } else {
                 //   e.preventDefault();
                 //   this.smoothScroll("home");
                //  }
               // }}
              >
                <span className="cd-dot" />
                <span className="cd-label">Home</span>
              </a>
            </li>
            <li>
              <a
                href="#about"
                data-number="2"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("about");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">About</span>
              </a>
            </li>
            <li>
              <a
                href="#consumers"
                data-number="3"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("consumers");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">Join the Movement</span>
              </a>
            </li>
            <li>
              <a
                href="#suppliers"
                data-number="4"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("suppliers");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">Waste Partners</span>
              </a>
            </li>
            <li>
              <a
                href="#social"
                data-number="5"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("social");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">Our Story</span>
              </a>
            </li>
            <li>
              <a
                href="#footer"
                data-number="6"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("footer");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">Contact</span>
              </a>
            </li>
            <li>
              <a
                href="#blogs"
                data-number="7"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("blogs");
                  }
                }}
              >
                {/*<span className="cd-dot" />
                <span className="cd-label"></span>*/}
              </a>
            </li>
            <li>
              <a
                href="#footer"
                data-number="8"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("footer");
                  }
                }}
              >
                {/*<span className="cd-dot" />
                <span className="cd-label"></span>*/}
              </a>
            </li>
            <li>
              <a
                href="#contacts"
                data-number="9"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("contacts");
                  }
                }}
              >
                {/*<span className="cd-dot" />
                <span className="cd-label"></span>*/}
              </a>
            </li>
          </ul>
        </nav>

         {/* <nav id="cd-vertical-nav">
            <ul>
              <li>
                <a
                  href="#home"
                  data-number="1"
                  className=""
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Home</span>
                </a>
              </li>
              <li>
                <a
                  href="#about"
                  data-number="2"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("about");
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">About</span>
                </a>
              </li>
              <li>
                <a
                  href="#consumers"
                  data-number="3"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("consumers");
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Consumers</span>
                </a>
              </li>
              <li>
                <a
                  href="#suppliers"
                  data-number="4"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("suppliers");
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Suppliers</span>
                </a>
              </li>
              <li>
                <a
                  href="#social"
                  data-number="5"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("social");
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Origins</span>
                </a>
              </li>
              <li>
                <a
                  href="#footer"
                  data-number="6"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("footer");
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Contact</span>
                </a>
              </li>
              <li>
                <a
                  href="#cards"
                  data-number="7"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("cards");
                  }}
                >
                  {/*<span className="cd-dot" />
                <span className="cd-label">Cards</span>
                </a>
              </li>
              <li>
                <a
                  href="#morphingCards"
                  data-number="8"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("morphingCards");
                  }}
                >
                  <span className="cd-dot" />
                <span className="cd-label">Morphing Cards</span>
                </a>
              </li>
              <li>
                <a
                  href="#javascriptComponents"
                  data-number="9"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("javascriptComponents");
                  }}
                >
                  <span className="cd-dot" />
                <span className="cd-label">Javascript</span>
                </a>
              </li>
            </ul>
          </nav>*/}<br></br><br></br>
          {/*(window.innerWidth < 512) ? <HomePageFooterCenter footer={footer} /> : <HomePageFooter footer={footer} />*/}
          <HomePageFooter footer={footer} />
        </div>
      );
    } else {
      return (
        <div id="home">
        <Splash />
          <Header
            brand="End Of Waste React"
            links={<HeaderLinks dropdownHoverColor="info" />}
            fixed
            color="transparent"
            changeColorOnScroll={{
              height: 400,
              color: "info"
            }}
          />
          {/*
          <Parallax
            image={require("assets/img/bg18-min.jpg")}
            className={classes.parallax}
          >
            <div className={classes.container}><br></br><br></br>
              <GridContainer>
              
                <GridItem xs={12} sm={12} md={7}>
                  <div className={classes.brand}>
                    <h1>
                    </h1>
                    <h3>
                    </h3>
                  </div>
                </GridItem>                  
                <GridItem xs={12} sm={12} md={5}><br></br>
                  <div className={classes.brand}>
                  <h2>
                  </h2>
                    <div>
                      <Button
                          onClick={e => {
                            e.preventDefault();
                            this.smoothScroll("about");
                          }}
                          color="info"
                          className={classes.navButton}
                          round
                        >
                        </Button>
                      </div>
                  </div>
                </GridItem>  
              </GridContainer>
            </div>
                        </Parallax>*/}
        
          <div
            className={classNames(classes.main, classes.mainRaised)}
            id="main-panel"
          >

            <div className={classes.about}>
              <About />
            </div>
            <Consumers />
            <Suppliers />
            <Social />
            <SectionCarousel />
            <Form />
            {/*<SectionBasics />
            <SectionNavbars />
            <SectionTabs />*/}
            {/*<div
              className={classNames(
                classes.section,
                classes.sectionGray,
                "cd-section"
              )}
              id="footers"
            >
              <SectionPreFooter />
              <SectionFooter />
              </div>
            <SectionTypography />
            <SectionCards />
            <SectionJavascript />*/}

          </div>
          <nav id="cd-vertical-nav">
            <ul>
              <li>
                <a
                  href="#home"
                  data-number="1"
                  className=""
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Home</span>
                </a>
              </li>
              <li>
                <a
                  href="#about"
                  data-number="2"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("about");
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">About</span>
                </a>
              </li>
              <li>
                <a
                  href="#consumers"
                  data-number="3"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("consumers");
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Consumers</span>
                </a>
              </li>
              <li>
                <a
                  href="#suppliers"
                  data-number="4"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("suppliers");
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Suppliers</span>
                </a>
              </li>
              <li>
                <a
                  href="#social"
                  data-number="5"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("social");
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Origins</span>
                </a>
              </li>
              <li>
                <a
                  href="#footer"
                  data-number="6"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("footer");
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Contact</span>
                </a>
              </li>
              <li>
                <a
                  href="#cards"
                  data-number="7"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("cards");
                  }}
                >
                  {/*<span className="cd-dot" />
                  <span className="cd-label">Cards</span>*/}
                </a>
              </li>
              <li>
                <a
                  href="#morphingCards"
                  data-number="8"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("morphingCards");
                  }}
                >
                  {/*<span className="cd-dot" />
                  <span className="cd-label">Morphing Cards</span>*/}
                </a>
              </li>
              <li>
                <a
                  href="#javascriptComponents"
                  data-number="9"
                  className=""
                  onClick={e => {
                    e.preventDefault();
                    this.smoothScroll("javascriptComponents");
                  }}
                >
                  {/*<span className="cd-dot" />
                  <span className="cd-label">Javascript</span>*/}
                </a>
              </li>
            </ul>
          </nav><br></br><br></br>
          {/*(window.innerWidth < 512) ? <HomePageFooterCenter /> : <HomePageFooter />*/}
        </div>
      )
    }
  }
}

const mapStateToProps = (state) => {
  //console.log(state);
  return {
    hero: state.firestore.ordered.hero,
    about: state.firestore.ordered.about,
    consumers: state.firestore.ordered.consumers,
    suppliers: state.firestore.ordered.suppliers,
    origins: state.firestore.ordered.origins,
    contact: state.firestore.ordered.contact,
    footer: state.firestore.ordered.footer,
    navbar: state.firestore.ordered.navbar,
  }
}

export default compose(
  connect(mapStateToProps),
  withStyles(homeStyle),
  firestoreConnect([
    {
      collection: 'hero',
      orderBy: 'order'
    },
    {
      collection: 'about',
      orderBy: 'order'
    },
    {
      collection: 'consumers',
      orderBy: 'order'
    },
    {
      collection: 'suppliers',
      orderBy: 'order'
    },
    {
      collection: 'origins',
      orderBy: 'order'
    },
    {
      collection: 'contact',
      orderBy: 'order'
    },
    {
      collection: 'footer',
      orderBy: 'order'
    },
    {
      collection: 'navbar',
      orderBy: 'order'
    }
  ])
)(Components)
